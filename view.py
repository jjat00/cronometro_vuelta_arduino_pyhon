import functools
import sys
import traceback
import pygame
import serial 
import copy
# for gui tools:
from PySide2 import QtCore
from PySide2 import QtGui
from PySide2 import *
from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtUiTools import *
from PySide2.QtUiTools import *
from PySide2 import QtUiTools

import pyqtgraph as pg

import numpy as np
#from PySide.QtGui import QVBoxLayout
from numpy import arange, sin, cos, pi

# aditional tools:
from functools import partial  # para facilitar el manejo de eventos


class View(QtCore.QObject):  # hereda de la clase QtGui.QmainWindow

    def __init__(self, ui_file_path, controller):

        # ------------basic configuration:------------------

        super(View, self).__init__(parent=None)  # initialize the parent class (QtGui)
        self.controller = controller

        # load ui file:
        loader = QtUiTools.QUiLoader()
        my_file = QtCore.QFile(ui_file_path)
        my_file.open(QtCore.QFile.ReadOnly)
        self.window = loader.load(my_file)
        my_file.close()

        self.window.start.clicked.connect(partial(self.start))
        #self.window.reset.clicked.connect(partial(self.reset))      

        self.window.show()
        print("view init done!")

    def start(self):
        
        N = 2
        ser = serial.Serial('COM6', baudrate=9600)
        ser.readline()
        is_running = True
        while is_running:           
             datoString = ser.readline()
             datoString.decode('utf-8')
             data_str = datoString.rstrip()
             data = int(data_str)
             print("return No: ",data)
             try:  #permite comprobar si hay un error al ejecutar la siguiente instrucción.
                if data == 1 :
                    pygame.init()
                    reloj = pygame.time.Clock()
                    t = pygame.time.get_ticks()/1000
                    print("time: ",t)
                if data == N:
                    print("time lap:",t)
                    self.window.Time.display(t)     
                    is_running = False   
             except UnicodeDecodeError: #Si se produce el error al plotear no hacemos nada y evitamos que el programa se pare.
                 pass       


        pygame.quit()